<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Endpoint for showing API status
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $categories = DB::table('product_categories')->get()->toArray();
        foreach ($categories as &$category) {

            $products = DB::table('products as p')
                ->join('manufacturers', 'manufacturers.id', '=', 'p.productManufacturerID')
                ->select('p.id','p.productSKU', 'p.productName','p.productPrice',
                    'p.productColor','p.productDescription','p.productQuantity',
                    'manufacturers.brandName')
                ->where('p.productCategoryID', $category->id)
                ->get()->toArray();
            foreach ($products as &$product){
                $options = DB::table('product_options')->where('productID', $product->id)->get(['size','material'])->toArray();
                $product->options = $options;
            }
            $category->products = $products;
        }
        $data = [
            'user' => Auth::user()->get()->toArray(),
            'items' => $categories
        ];
        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => $data
        ], 200);
    }


}
