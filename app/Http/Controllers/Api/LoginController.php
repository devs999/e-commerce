<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as Controller;
use App\User;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login(Request $request) {
        // Getting all post data
        $data = $request->input();
        // Applying validation rules.
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|min:6',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
            // If validation falis redirect back to login.
            if ($validator->fails()) {
                return response()->json($validator->messages(), 401);
            }
        }
        else {
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

//            // doing login.
            if (Auth::validate($userdata)) {
                    Auth::attempt($userdata);
                    $email = $request->input('email');
                    $user = User::where('email', $email)->first();
                    $user->api_token = str_random(60);
                    $user->save();
                    return response()->json([
                        'token' => $user->api_token,
                        'status' => 'success'
                    ], 200);
            }
            else {
                // if any error send back with message.
                return response()->json([
                    'message' => 'Please check your credentials.',
                    'status' => 'error'
                ], 401);
            }
        }
    }
}

