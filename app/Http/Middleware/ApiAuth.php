<?php

namespace App\Http\Middleware;

use Closure;
use App\User as User;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Support\Facades\Auth as Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Init potential error message
        $error = '';

        // Try to get token from headers_list
        $token = $request->header('token', '');

        // If still empty, try from request query/body
        if (empty($token)) {
            $token = $request->get('token', '');
        }

        // Proceed to get user if token only
        if (!empty($token)) {

            // Try to get from DB and authenticate
            try {
                $user = User::where('api_token', '=', $token)->firstOrFail();
                Auth::login($user);

            // Not found
            } catch (ModelNotFoundException $e) {
                $error = 'Invalid Token';
            }

        // No token provided
        } else {
            $error = 'No API Token Provided';
        }

        // If got error, show it with 401
        if (!empty($error)) {
            return response()->json([
                'error' => $error,
                'status' => 401
            ], 401);
        }

        // Authenticated, proceed
        return $next($request);
    }
}
