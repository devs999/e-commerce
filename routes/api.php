<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** API Routes **/

Route::post('login', 'Api\LoginController@login');

Route::group(['middleware' => 'apiAuth'], function () {
//    Route::get('test', 'Api\DefaultController@status');
    Route::get('getData', 'Api\ApiController@getData');
});

