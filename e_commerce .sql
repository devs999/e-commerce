-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 28 2017 г., 13:54
-- Версия сервера: 10.1.28-MariaDB
-- Версия PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `e_commerce`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categorymanufacturer`
--

CREATE TABLE `categorymanufacturer` (
  `id` int(11) NOT NULL,
  `manufacturerID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categorymanufacturer`
--

INSERT INTO `categorymanufacturer` (`id`, `manufacturerID`, `categoryID`) VALUES
(1, 1, 2),
(2, 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL,
  `brandName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `brandName`) VALUES
(1, 'Armani'),
(2, 'D&G');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `orderUserID` int(11) NOT NULL,
  `orderAmount` float NOT NULL,
  `orderShipName` varchar(255) NOT NULL,
  `orderShipAddress` varchar(255) NOT NULL,
  `orderEmail` varchar(255) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `orderShipped` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `detailOrderID` int(11) NOT NULL,
  `detailProductID` int(11) NOT NULL,
  `detailName` varchar(255) NOT NULL,
  `detailPrice` float NOT NULL,
  `detailSKU` varchar(255) NOT NULL,
  `detailQuantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `productSKU` varchar(255) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `productPrice` float NOT NULL,
  `productColor` varchar(255) NOT NULL,
  `productDescription` varchar(255) NOT NULL,
  `productCategoryID` int(11) DEFAULT NULL,
  `productManufacturerID` int(11) DEFAULT NULL,
  `productDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `productQuantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `productSKU`, `productName`, `productPrice`, `productColor`, `productDescription`, `productCategoryID`, `productManufacturerID`, `productDate`, `productQuantity`) VALUES
(1, '000-0001', 'Cotton T-Shirt', 9.99, 'Black', 'Light Cotton T-Shirt', 2, 1, '2013-06-12 21:00:50', 100),
(2, '000-0004', 'Breeches', 179.99, 'Red', 'Track and Trail', 3, 2, '2013-07-25 15:04:36', 5),
(3, '000-00057', 'Dress', 179.99, 'White', 'Short Dress', 3, 2, '2013-07-25 15:04:36', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_categories`
--

INSERT INTO `product_categories` (`id`, `categoryName`, `parentID`) VALUES
(1, 'Woman', 0),
(2, 'Man', 0),
(3, 'Kids', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `product_options`
--

CREATE TABLE `product_options` (
  `id` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `material` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_options`
--

INSERT INTO `product_options` (`id`, `productID`, `material`, `size`) VALUES
(1, 1, 'Cotton', 'M'),
(2, 1, 'Cotton', 'S'),
(3, 1, 'Wool', 'XS'),
(4, 2, 'Cotton', 'L'),
(5, 3, 'Wool', 'S');

-- --------------------------------------------------------

--
-- Структура таблицы `shopproducts`
--

CREATE TABLE `shopproducts` (
  `id` int(11) NOT NULL,
  `shopID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `productPrice` float NOT NULL,
  `productQuantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shopproducts`
--

INSERT INTO `shopproducts` (`id`, `shopID`, `productID`, `productPrice`, `productQuantity`) VALUES
(1, 2, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `shopName` varchar(255) NOT NULL,
  `shopAddress` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shops`
--

INSERT INTO `shops` (`id`, `shopName`, `shopAddress`) VALUES
(1, 'shop store', 'address 1'),
(2, 'shop center', 'address 2');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'testyan@gmail.com', '$2y$10$v6P8n3fzuOUu8BSQbpjlHe0I4Ybb4gbkFoa7AEI3XbNmn6O/Q5f0S', 'VmpUnmwFlH9ZZnI3DM1ukmKbQgw07VbFsFeG78k7M0w4PkNpwLMoHXHQDBpl', NULL, '2017-11-27 11:43:56', '2017-11-27 11:50:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categorymanufacturer`
--
ALTER TABLE `categorymanufacturer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_categorymanufacturerId` (`manufacturerID`),
  ADD KEY `FK_categorymanufacturer` (`categoryID`);

--
-- Индексы таблицы `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_order_detailsID` (`detailOrderID`),
  ADD KEY `FK_order_detailsProdID` (`detailProductID`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_productCatId` (`productCategoryID`),
  ADD KEY `FK_productsBrandId` (`productManufacturerID`);

--
-- Индексы таблицы `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_productID1` (`productID`);

--
-- Индексы таблицы `shopproducts`
--
ALTER TABLE `shopproducts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_shopproducts` (`productID`),
  ADD KEY `FK_shopID` (`shopID`);

--
-- Индексы таблицы `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categorymanufacturer`
--
ALTER TABLE `categorymanufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `shopproducts`
--
ALTER TABLE `shopproducts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categorymanufacturer`
--
ALTER TABLE `categorymanufacturer`
  ADD CONSTRAINT `FK_categorymanufacturer` FOREIGN KEY (`categoryID`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `FK_categorymanufacturerId` FOREIGN KEY (`manufacturerID`) REFERENCES `manufacturers` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK_order_detailsID` FOREIGN KEY (`detailOrderID`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `FK_order_detailsProdID` FOREIGN KEY (`detailProductID`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_productCatId` FOREIGN KEY (`productCategoryID`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `FK_productsBrandId` FOREIGN KEY (`productManufacturerID`) REFERENCES `manufacturers` (`id`);

--
-- Ограничения внешнего ключа таблицы `product_options`
--
ALTER TABLE `product_options`
  ADD CONSTRAINT `FK_productID1` FOREIGN KEY (`productID`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `shopproducts`
--
ALTER TABLE `shopproducts`
  ADD CONSTRAINT `FK_shopID` FOREIGN KEY (`shopID`) REFERENCES `shops` (`id`),
  ADD CONSTRAINT `FK_shopproducts` FOREIGN KEY (`productID`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
